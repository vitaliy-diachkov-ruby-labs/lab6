class CalculatorController < ApplicationController
  skip_before_action :verify_authenticity_token

  def initialize
    @items = [
        {:name => "Shirt", :price => 500},
        {:name => "Jeans", :price => 450},
        {:name => "Shoes", :price => 380}
    ]
    @result = 0
    super
  end

  def index
  end

  def calculate
    puts params.keys.to_s

    result = 0

    @items.each do |item|
      params.keys.each do |key|
        if key.to_s == item[:name].to_s and params[key].to_i >= 0
          result += item[:price].to_i * params[key].to_i
        end
      end
    end

    respond_to do |format|
      format.json { render json: {:result => result} }
    end
  end
end
